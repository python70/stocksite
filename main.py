#! /bin/python3

import requests
import os
import sys
import json

from stock import Stock
from dotenv import load_dotenv 

load_dotenv()
sym = sys.argv[1]
key = os.getenv('KEY')
url = url = "https://www.alphavantage.co/query?function=OVERVIEW&symbol={}&apikey={}".format(sym,key)
r = requests.get(url)
data = r.json()

s = Stock()
s\
.set_name(data["Name"])\
.set_eps(data["EPS"])\
.set_mv200(data["200DayMovingAverage"])\
.set_mv50(data["50DayMovingAverage"])\
.set_dividend(data["DividendPerShare"])\
.set_div_yield(data["DividendYield"])\
.set_fwdpe(data["ForwardPE"])\
.set_bvps(data["BookValue"])

s.print()

