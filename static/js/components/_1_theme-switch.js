(function() {
  var themeSelect = document.getElementById('select-theme');
  if(themeSelect) {
    var htmlElement = document.getElementsByTagName("html")[0];
    initTheme();
    
    themeSelect.addEventListener('change', function(event){
      resetTheme(themeSelect.value);
    });

    function initTheme() { // update the <select> element to show the selected color theme
      var dataTheme = htmlElement.getAttribute('data-theme');
      if(dataTheme) themeSelect.value = dataTheme;
    };

    function resetTheme(theme) {
      if(theme) {
        htmlElement.setAttribute('data-theme', theme);
        localStorage.setItem('colorTheme', theme);
      }
    };
  }
}());