# make formal with getters and setters
# then make store in db, if look up is older than day, pull new
# otherwise print db data

class Stock:

    def set_name(self, name):
        self.name = name
        return self;

    def set_eps(self, eps):
        self.eps = eps
        return self;

    def set_mv200(self, mv200):
        self.mv200 = mv200
        return self;

    def set_mv50(self, mv50):
        self.mv50 = mv50
        return self;
        
    def set_dividend(self, dividend):
        self.dividend = dividend
        return self;

    def set_div_yield(self, div_yield):
        self.div_yield = div_yield
        return self;

    def set_fwdpe(self, fwdpe):
        self.fwdpe = fwdpe 
        return self;

    def set_bvps(self, bvps):
        self.bvps = bvps
        return self;

    def print(self):
        return "<p>Name: " + self.name + "</p>" +\
        "<p>EPS: " + self.eps + "</p>" +\
        "<p>200 Day Moving average: " + self.mv200 + "</p>" +\
        "<p>50 Day Moving average: " + self.mv50 + "</p>" +\
        "<p>Dividend: " + self.dividend + "</p>" +\
        "<p>Dividend Yield: " + self.div_yield + "</p>" +\
        "<p>FWD PE: " + self.fwdpe + "</p>" +\
        "<p>BVPS: " + self.bvps + "</p>"

