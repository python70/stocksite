from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader
from .forms import StockForm

import requests
import os
import json

from .classes.stock import Stock
from dotenv import load_dotenv 

# Create your views here.
def index(request):
    return render(request, "stocks/index.html")

def search(request):
    load_dotenv()
    stock = StockForm(request.POST)

    data = {}
    symb = "Invalid Ticker"
    info = ""

    if stock.is_valid():
        symb = stock.cleaned_data['symb']
        key = os.getenv('KEY')
        url = url = "https://www.alphavantage.co/query?function=OVERVIEW&symbol={}&apikey={}".format(symb,key)
        r = requests.get(url)
        data = r.json()
        if data != {}:
            s = Stock()
            s\
            .set_name(data["Name"])\
            .set_eps(data["EPS"])\
            .set_mv200(data["200DayMovingAverage"])\
            .set_mv50(data["50DayMovingAverage"])\
            .set_dividend(data["DividendPerShare"])\
            .set_div_yield(data["DividendYield"])\
            .set_fwdpe(data["ForwardPE"])\
            .set_bvps(data["BookValue"])
            info = s.print()
        else:
            info = "Ticker not found"
    else:
        symb = "Invalid Ticker"

    data['symb'] = symb
    data['info'] = info 

    return render(request, "stocks/index.html", data)
